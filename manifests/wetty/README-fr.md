# WeTTY

Terminal via HTTP et https.
WeTTY est une meilleure alternative à ajaxterm car WeTTY utilise xterm.js qui est une implémentation à part entière de l'émulation de terminal entièrement écrit en JavaScript.
WeTTY utilise des websockets plutôt qu'Ajax et donc obtien un meilleur temps de réponse
