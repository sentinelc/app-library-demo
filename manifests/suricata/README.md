# Suricata IDS

Suricata is a high performance, open source network analysis and threat detection
software used by most private and public organizations, and embedded by major
vendors to protect their assets.

## How to use this service

### Selecting an interface to monitor

This recipe uses host network mode, so suricata has access to all network
interfaces of the host.

The only required parameter is the network interface name to monitor. It defaults
to `br0` which is the name of the bridge on Sentinelc OS where all network
traffic transits.

To monitor another interface, you will need to know and enter its linux interface
name.


### Streaming logs to Elastic Search

An optional fluent-bit container can be started to live stream logs to elastic search.

Enable the option and enter your elastic search endpoint and credentials in the provided
settings.

Under Stack management of your elastic search cluster:

- Create a `suricata` role that has the `create_doc` and `auto_configure` permissions on
  `suricata-*` indices.
- Create an elastic search user for each suricata instances with a unique username/password
  and the `suricata` role.

When installing the suricata service:

- Enable fluent-bit live log streaming.
- Enter the host/port of your elastic search cluster.
- Enter the credentials, either a username/password or a Cloud ID/Cloud Auth if using elastic cloud.
- Enter `suricata` as the index prefix.
- Enable TLS, TLS verification and paste the CA certificate of your elastic cluster instance.

## Known issues

### Log file rotation

Log files are generated in a local volume named 'logs'. At this moment, the log files
are not rotated and will need to be managed manually.

To rotate log files manually:

```bash
podman exec [CONTAINER_ID] logrotate /etc/logrotate.d/suricata
```

### Refreshing rules

Suricata will download default rulesets on the first start but the rules are not 
refreshed automatically.

To refresh the rules manually, run the following command:

```bash
podman exec -it --user suricata [CONTAINER_ID] suricata-update -f
```
