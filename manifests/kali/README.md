# Kali Linux adapted for SentinelC

What is a penetration testing distribution? As legend tells it, years ago there was a penetration test in an isolated environment where the assessment team was not able to bring in any computers or have network access in or out of the target environment. In order to do the work, the first penetration testing distribution was born. It was a bootable Live CD configured with various tools needed to do the work, and after the assessment was completed the Live CD was shared online and became very popular.

What are the makings of a great penetration testing distribution? What a penetration testing distribution is judged on has changed over the years. Originally it was just how many tools did it hold. Then it was did it have package management, and how often were they updated? As we have matured, so has the features that we need to provide. The true indication of a great Penetration Testing distribution is the selection of useful and relevant features that it offers security professionals. What kind of features are we talking about? We’re happy you asked! We’ve made a short list of some of these Kali Linux features, and linked them to their respective sources.

Please note, those are the basics tools install with this version adapted for SentinelC:
- ``` kali-linux-headless ```
- ``` openssh-server ```
- ``` whois ```
- ``` nano ```

If you want to add differents tools use the following
- ```apt-get -y install <package>```