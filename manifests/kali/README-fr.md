# Kali Linux

Qu'est-ce qu'une distribution de tests d'intrusion ? Comme le raconte la légende, il y a des années, il y a eu un test d'intrusion dans un environnement isolé où l'équipe d'évaluation n'a pas été en mesure d'apporter des ordinateurs ou d'avoir un accès au réseau dans ou hors de l'environnement cible. Afin de faire le travail, la première distribution de tests d'intrusion est née. Il s'agissait d'un Live CD amorçable configuré avec divers outils nécessaires pour effectuer le travail, et une fois l'évaluation terminée, le Live CD a été partagé en ligne et est devenu très populaire.

Quels sont les ingrédients d'une excellente distribution de tests d'intrusion ? Ce sur quoi une distribution de tests d'intrusion est jugée a changé au fil des ans. À l'origine, c'était juste le nombre d'outils qu'il contenait. Ensuite, il y avait-il une gestion des paquets et à quelle fréquence étaient-ils mis à jour ? Au fur et à mesure que nous avons mûri, les fonctionnalités que nous devons fournir ont également évolué. La véritable indication d'une excellente distribution de tests d'intrusion est la sélection de fonctionnalités utiles et pertinentes qu'elle offre aux professionnels de la sécurité. De quel type de fonctionnalités parle-t-on ? Nous sommes heureux que vous ayez demandé ! Nous avons dressé une courte liste de certaines de ces fonctionnalités de Kali Linux et les avons liées à leurs sources respectives.

Prenez note qu'il n'y a pas d'outils par defaut. Utilisez les commandes suivantes afin d'en installer:
- ```bash apt update && apt -y install <package>```
- ```bash apt update && apt -y install kali-linux-headless```
- ```bash apt update && apt -y install kali-linux-large```

