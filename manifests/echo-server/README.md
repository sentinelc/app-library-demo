# Echo-Server

An echo server is a server that replicates the request sent by the client and sends it back.



Available features:
- GET / POST / PUT / PATCH / DELETE
- Request (Query, Body, IPs, Host, Urls…)
- Request Headers / Response Headers
- Environment variables
- Control via Headers/Query
- Folders and Files
- Monitoring


## Custom responses

|Query|Header|Content|Conditions|
|--- |--- |--- |--- |
|?echo_code=|X-ECHO-CODE|HTTP code 200, 404|200 <= CODE <= 599|
|||404-401 or 200-500-301||
|?echo_body=|X-ECHO-BODY|Body message||
|?echo_env_body=|X-ECHO-ENV-BODY|The key of environment variable|Enable environment true|
|?echo_header=|X-ECHO-HEADER|Response Header Lang: en-US|Enable header true|
|?echo_time=|X-ECHO-TIME|Wait time in ms|0 <= TIME <= 30.000|
|?echo_file=|X-ECHO-FILE|Path of Directory or File|Enable file true|