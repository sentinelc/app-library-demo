A simple preconfigured nginx http proxy that can safely open access to a local web server running 
inside the LAN to the cloud.

For example lets say you have an HTTP management console of some random appliance
running internally at `http://192.168.77.33`. Install an instance of this service
in the same zone as your appliance is running, and set the Proxy pass location
config to `192.168.77.33:80`.

Use the 'Link to web based interface' that will appear in the sentinelc service
details to securely access the web interface remotely. 

## Advanced configuration

Refer to the built-in nginx.conf to understand where the parameters you provide
at installation will go. The only required parameter is the upstream HTTP server 
location (PROXY_PASS).

https://gitlab.com/sentinelc/containers/nginx-proxy/-/blob/main/nginx.conf.template?ref_type=heads
