# MariaDB

OpenVAS est un outil de détection de vulnérabilité complet. Ses capacités incluent des tests non authentifiés et authentifiés, divers protocoles Internet et industriels de haut et bas niveau, un réglage des performances pour les analyses à grande échelle et un puissant langage de programmation interne pour mettre en œuvre tout type de test de vulnérabilité.
L'outil de détection obtient les tests de détection des vulnérabilités à partir d'un flux qui a un long historique et des mises à jour quotidiennes.

OpenVAS a été développé et propulsé par la société Greenbone Networks depuis 2006. Dans le cadre de la famille de produits commerciaux de gestion des vulnérabilités "Greenbone Security Manager" (GSM), l'outil de détection forme la gestion des vulnérabilités Greenbone avec d'autres modules Open Source.

